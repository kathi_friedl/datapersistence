# **JDBC und DAO - Videotraining**

**1. Aufgabe 3:** [Videotraining durcharbeiten](#aufgabe-3)<br>
**2. Aufgabe 4:** [System um Student-Entität erweitern](#aufgabe-4)<br>
**3. Aufgabe 5:** [Textuelle Beschreibung über Erweiterung durch Buchungs-Entität](#aufgabe-5)<br><br>

---
### **Aufgabe 3**

Lerne anhand des zweiten Videotrainings, wie man mit JAVA auf Daten aus Datenbanken mit dem DAOEntwurfsmuster zugreift.
- Programmiere alles selbstständig nach und mit!
- Spiele ein wenig mit dem Code, probiere aus!• Du musst alle Codeteile verstehen und syntaktisch sowie semantisch erklären können
- Du musst neue Konzepte verstehen und erklären können und diese selbst dokumentieren (Markdown). Zu diesen Konzepten zählen:
  - DAO-Pattern zum objektrelationalen Zugriff auf Datenbanken verstehen und anwenden
  - Grundkonzept des objektrelationalen Mappings verstehen
  - Singleton-Pattern zum Aufbau der DB-Verbindung verstehen und anwenden
  - Commandline-Interface (Kommandozeilenmenü) sauber programmieren
  - Exceptions verstehen und verwenden
  - Abstrakte Klassen verstehen und verwenden
  - Interfaces (auch mit Erben für Interfaces) verstehen und verwenden
  - Domänenklassen korrekt aufbauen (Objekte immer im konsistenten Zustand halten, Exceptions verwenden, Setter absichern)
  - CRUD-Operationen mit DAO-Pattern und JDBC umsetzen

---
<br>


## **1 Einführung**

![DAO](img/DAO.png)
   

## **2 Projektsetup**
    - Neue Tabelle mit entsprechenden Spalten in der Datenbank erstellen
    - Maven-Projekt in IntelliJ erstellen
    - Dependency einfügen
      ```
      <dependency>
          <groupId>mysql</groupId>
          <artifactId>mysql-connector-java</artifactId>
          <version>8.0.28</version>
      </dependency>
      ```

## **3 Datenbankverbindung Singleton**
   
   Dafür wird eine neue Klasse `MysqlDatabaseConnection` erstellt, bei welcher der Konstruktor privat ist. Somit kann kein neues Objekt dieser Klasse erstellt werden. Um die Verbindung aus der Variablen `con` zurückzubekommen, wird eine Getter-Methode `getConnection` benutzt, welche die Verbindung zurückgibt. Somit können nicht mehrere Verbindungen erstellt werden sondern einzig und allein diese eine.

    ```
    public class MysqlDatabaseConnection {
    
        private static Connection con = null;

        private MysqlDatabaseConnection(){
        }

        public static Connection getConnection(String url, String user, String pwd throws ClassNotFoundException, SQLException {
        
            if(con!=null){
                return con;
            }else{
                Class.forName("com.mysql.cj.jdbc.Driver");
                con = DriverManager.getConnection(url, user, pwd);
                return con;
            }

        }
    }
    ```
## **4 User Interface**

  **CLI-Klasse:**

    ```
      public class Cli {

      Scanner scan;
      MyCourseRepository courseRepo;
      MyStudentRepository studentRepo;

      public Cli(MyCourseRepository courseRepo, MyStudentRepository studentRepo) {
          this.scan = new Scanner(System.in);
          this.courseRepo = courseRepo;
          this.studentRepo = studentRepo;
      }

      public void start() {
          String input = "-";
          while (!input.equals(("x"))) {
              showMenue();
              input = scan.nextLine();
              switch (input) {
                  case "1":
                      // Aktion
                      break;
                  case "2":
                      // Aktion 
                      break;

                  case "x":
                      System.out.println("Auf Wiedersehen!");
                      break;
                  default:
                      inputError();
                      break;
              }
        }
        scan.close();
      }

      private void showCourseMenue() {
          System.out.println("----------------- Kursverwaltung -----------------");
          System.out.println("(1) Kurs eingeben \t (2) Alle Kurse anzeigen \t (3) Kursdetails anzeigen");
          System.out.println("(4) Kursdetails ändern \t (5) Kurs löschen \t (6) Kurs suchen");
          System.out.println("(7) Laufende Kurse \t (-) xxxxxx \t (-) xxxxxx");
          System.out.println("(x) ENDE");
      }

      }
    ```

## **5 Domänenklassen**

  Für die Tabelle "course" in der Datenbank wird eine entsprechende Domänenklasse benötigt. Dazu wird im package "domain" eine neue Klasse `Course` erstellt. Die Datenfelder dieser Klasse stimmen mit den Spalten in der Datenbanktabelle überein. <br>
  
  Ein Interface "BaseEntity", welches von den Domänenklassen implementiert werden soll, hilft dabei, Codeduplikate zu vermeiden.

## **6 DAO Interfaces**

1. Interface `public interface BaseRepository<T, I>`: Hierbei muss der Entitätstyp und der Typ der ID angegeben werden Basis-CRUD-Methoden werden definiert
2. Interface `public interface MyCourseRepository extends BaseRepository<Course, Long>`: Dieses Interface erbt vom Interface `BaseRepository` und definiert die benötigten Typen, mit welchen gearbeitet wird; zusätzliche entitätsspezifische Methoden werden definiert
3. Klasse `public class MySqlCourseRepository implements MyCourseRepository`: Dies ist die implementierende Klasse; alle von den Interfaces vordefinierten Methoden werden ausimplementiert


## **7 Get All**

```
    @Override
    public List<Course> getAll() {
        String sql = "SELECT * FROM `courses`";
        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Course> courseList = new ArrayList<>();
            while (resultSet.next()) {
                courseList.add(new Course(
                                resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getString("description"),
                                resultSet.getInt("hours"),
                                resultSet.getDate("begindate"),
                                resultSet.getDate("enddate"),
                                CourseType.valueOf(resultSet.getString("coursetype"))
                        )
                );
            }
            return courseList;
        } catch (SQLException e) {
            throw new DatabaseException("Database error occured");
        }
    }
```

## **8 Get by ID**

```
@Override
    public Optional<Course> getById(Long id) {
        Assert.notNull(id);
        if (countCoursesInDbWithId(id) == 0) {
            return Optional.empty();
        } else {
            try {
                String sql = "SELECT * FROM `courses` WHERE `id` = ?";
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();

                Course course = new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("begindate"),
                        resultSet.getDate("enddate"),
                        CourseType.valueOf(resultSet.getString("courseType"))
                );
                return Optional.of(course);
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }

        }
    }

```

## **9 Create**

```
 @Override
    public Optional<Course> insert(Course entity) {
        Assert.notNull(entity);
        try {
            String sql = "INSERT INTO `courses` (`name`, `description`, `hours`, `begindate`, `enddate`, `coursetype`) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getDescription());
            preparedStatement.setInt(3, entity.getHours());
            preparedStatement.setDate(4, entity.getBeginDate());
            preparedStatement.setDate(5, entity.getEndDate());
            preparedStatement.setString(6, entity.getCourseType().toString());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                return Optional.empty();
            }
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return this.getById(generatedKeys.getLong(1));
            } else {
                return Optional.empty();
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }

    }

```

## **10 Update**
```
    @Override
    public Optional<Course> update(Course entity) {
        Assert.notNull(entity);

        if (countCoursesInDbWithId(entity.getId()) == 0) {
            return Optional.empty();
        } else {
            try {
                String sql = "UPDATE `courses` SET `name` = ?, `description` = ?, `hours` = ?, `begindate` = ?, `enddate` = ?, `coursetype` = ? WHERE `courses`.`id` = ?";
                PreparedStatement preparedStatement = con.prepareStatement(sql);

                preparedStatement.setString(1, entity.getName());
                preparedStatement.setString(2, entity.getDescription());
                preparedStatement.setInt(3, entity.getHours());
                preparedStatement.setDate(4, entity.getBeginDate());
                preparedStatement.setDate(5, entity.getEndDate());
                preparedStatement.setString(6, entity.getCourseType().toString());
                preparedStatement.setLong(7, entity.getId());

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    return Optional.empty();
                } else {
                    return this.getById(entity.getId());
                }
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }
```

## **11 Delete**
```
    @Override
    public void deleteById(Long id) {
        Assert.notNull(id);
        String sql = "DELETE FROM `courses` WHERE `id` = ?";

        try {
            if (countCoursesInDbWithId(id) == 1) {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

```

## **12 Course Search**
```
  @Override
    public List<Course> findAllCoursesByName(String name) {
        try {
            String sql = "SELECT * FROM `courses` WHERE LOWER(`name`) LIKE LOWER(?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%" + name + "%");

            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Course> courseList = new ArrayList<>();
            while (resultSet.next()) {
                courseList.add(new Course(
                                resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getString("description"),
                                resultSet.getInt("hours"),
                                resultSet.getDate("begindate"),
                                resultSet.getDate("enddate"),
                                CourseType.valueOf(resultSet.getString("coursetype"))
                        )
                );
            }
            return courseList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }
```

```

    @Override
    public List<Course> findAllCoursesByDescription(String description) {
        try {
            String sql = "SELECT * FROM `courses` WHERE LOWER(`description`) LIKE LOWER(?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%" + description + "%");

            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Course> courseList = new ArrayList<>();
            while (resultSet.next()) {
                courseList.add(new Course(
                                resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getString("description"),
                                resultSet.getInt("hours"),
                                resultSet.getDate("begindate"),
                                resultSet.getDate("enddate"),
                                CourseType.valueOf(resultSet.getString("coursetype"))
                        )
                );
            }
            return courseList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

```


```
    @Override
    public List<Course> findAllCoursesByNameOrDescription(String searchText) {

        try {
            String sql = "SELECT * FROM `courses` WHERE LOWER(`description`) LIKE LOWER(?) OR LOWER(`description`) LIKE LOWER(?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%" + searchText + "%");
            preparedStatement.setString(2, "%" + searchText + "%");

            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Course> courseList = new ArrayList<>();
            while (resultSet.next()) {
                courseList.add(new Course(
                                resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getString("description"),
                                resultSet.getInt("hours"),
                                resultSet.getDate("begindate"),
                                resultSet.getDate("enddate"),
                                CourseType.valueOf(resultSet.getString("coursetype"))
                        )
                );
            }
            return courseList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

```

## **13 Running Courses**

```
    @Override
    public List<Course> findAllRunningCourses() {
        try {
            String sql = "SELECT * FROM `courses` WHERE NOW()<`enddate`";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Course> courseList = new ArrayList<>();
            while (resultSet.next()) {
                courseList.add(new Course(
                                resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getString("description"),
                                resultSet.getInt("hours"),
                                resultSet.getDate("begindate"),
                                resultSet.getDate("enddate"),
                                CourseType.valueOf(resultSet.getString("coursetype"))
                        )
                );
            }
            return courseList;
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }
```


---

### **Aufgabe 4**

Erweitere die fertig nachprogrammierte Applikation mit einem DAO für CRUD für eine neue Domänenklasse „Student“:
- Studenten haben einen eine Student-ID, einen VN, einen NN, ein Geburtsdatum
- Domänenklasse implementieren (Setter absichern, neue Exceptions definieren, Business-Regeln selbst wählen - z.B. dass Name nicht leer sein darf)
- Eigenes Interface MyStudentRepository von BaseRepository ableiten. MyStudentRepository muss mindestens 3 studentenspezifische Methoden enthalten (z. B. Studentensuche nach Namen, Suche nach ID, Suche nach bestimmtem Geburtsjahr, Suche mit Geburtsdatum zwischen x und y etc.).
- Implementierung von MyStudentRepository durch eine neue Klasse MySqlStudentRepository analog
- Erweiterung des CLI für die Verarbeitung von Studenten und für spezifische Studenten-Funktionen (z.B. Student nach dem Namen suchen)
---

**Vorgehensweise:**
1. Interface `MyStudentRepository` erstellen und von `BaseRepository` ableiten
2. benötigte Methoden definieren
3. Klasse `MySqlStudentRepository` ausimplementieren
4. CLI erweitern


**>>> siehe CODE <<<**





---

### **Aufgabe 5**

Gib einen textuellen Vorschlag hab, wie man die bisher programmierte Applikation für die Buchung von Kursen durch Studenten erweitern könnte.

Beschreibe, wie eine neue Buchungs-Domänenklasse ausschauen sollte, wie man ein DAO für Buchungen dazu entwickeln sollte, wie man die CLI anpassen müsste und welche Anwendungsfälle der Benutzer brauchen könnte (wie etwa „Buchung erstellen“).

Verwende zur Illustration insb. auch UML-Diagramme.

---
<br>

## **Vorschlag für Erweiterung mit Buchungs-Entität**

Da zwischen der Kurs- und der Studentenentität eine m:n Beziehung herrscht, wird eine Zwischentabelle benötigt. Diese Zwischentabelle kann durch die neue Entität "Buchung"  dargestellt werden. Sie enthält den Kurs, der gebucht wird, den Studenten, welcher diesen Kurs bucht und den Zeitpunkt, in welchem die Buchung stattfindet. <br>

![Beziehung Kursmanagement](img/BeziehungKursmanagement.png)


Im Bezug auf die Datenbank wird dementsprechend eine neue Tabelle mit den zugehörigen Spalten sowie eine neue Entität mit entsprechenden Datenfeldern im Java-Code benötigt.
Unter Berücksichtigung der Verwendung des DAO-Entwurfsmusters wird ein neues Interface (z. B. MyBookingRepository) benötigt, welches vom BaseRepository-Interface erbt und zusätzliche, auf die Booking-Entität spezialisierte Methoden definiert, die neben den grundlegenden CRUD-Funktionen gebraucht werden. Diese könnten zum Beispiel sein:
- getAllBookingsByStudent()
- getAllBookingsByCourse()
- getAllBookingsByDate()
- getAllFutureBookings()
- getAllPastBookings()
- getallRunningBookings()

Diese Funktionen werden in einer Klasse ausimplementiert, welche dieses Interface implementiert und beispielsweise "MySqlBookingsRepository" heißen könnte.
Die Methoden aus dieser Klasse können in weiterer Folge auch für die CLI-Funktionen verwendet werden.

**Vorgehensweise:**
1. Am Datenbankserver eine neue Datenbank namens "bookings" erstellen
2. Spalten hinzufügen: Student, Kurs, Zeitpunkt
3. Neue Entität "Booking" im Package "domain" erstellen
4. Datenfelder entsprechend der Spalten der Datenbanktabelle erstellen
5. Interface als Vorlage der zu implementierenden Methoden erstellen; dieses Interface erbt vom Basisinterface
6. Klasse erstellen, welche die Methoden ausimplementiert
7. CLI um die Buchungsfunktionen erweitern


In der folgenden Abbildung ist das entsprechende UML-Diagramm zu meinem Implementierungsvorschlag zu erkennen:



![UML_Kursmanagement](img/Kursmanagement_UML.png)












