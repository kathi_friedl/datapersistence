package dataaccess;

import domain.Course;
import domain.Student;

import java.sql.Date;
import java.util.List;

public interface MyStudentRepository extends BaseRepository<Student, Long>{

    List<Student> findAllStudentsByPrename(String vorname);
    List<Student> findAllStudentsByLastname(String lastname);
    List<Student> findAllStudentsByBirthDate(Date birthdate);
    List<Student> findAllStudentsBetweenTwoBirthdates(Date begin, Date end);

}
