package ui;

import dataaccess.DatabaseException;
import dataaccess.MyCourseRepository;
import dataaccess.MyStudentRepository;
import domain.Course;
import domain.CourseType;
import domain.InvalidValueException;
import domain.Student;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Cli {

    Scanner scan;
    MyCourseRepository courseRepo;
    MyStudentRepository studentRepo;

    public Cli(MyCourseRepository courseRepo, MyStudentRepository studentRepo) {
        this.scan = new Scanner(System.in);
        this.courseRepo = courseRepo;
        this.studentRepo = studentRepo;
    }

    public void start() {
        String input = "-";
        while (!input.equals(("x"))) {
            showMenue();
            input = scan.nextLine();
            switch (input) {
                case "1":
                    startKursverwaltung();
                    break;
                case "2":
                    startStudentenverwaltung();
                    break;

                case "x":
                    System.out.println("Auf Wiedersehen!");
                    break;
                default:
                    inputError();
                    break;
            }
        }
        scan.close();
        System.exit(0);
    }

    private void startStudentenverwaltung() {
            String input = "-";
            while (!input.equals(("x"))) {
                showStudentMenue();
                input = scan.nextLine();
                switch (input) {
                    case "1":
                        addStudent();
                        break;
                    case "2":
                        showAllStudents();
                        break;
                    case "3":
                        showStudentDetails();
                        break;
                    case "4":
                        updateStudentDetails();
                        break;
                    case "5":
                        deleteStudent();
                        break;
                    case "6":
                        searchStudentByPrename();
                        break;
                    case "x":
                        start();
                        break;
                    default:
                        inputError();
                        break;
                }
            }
    }


    private void startKursverwaltung() {
        String input = "-";
        while (!input.equals(("x"))) {
            showCourseMenue();
            input = scan.nextLine();
            switch (input) {
                case "1":
                    System.out.println("Kurseingabe");
                    addCourse();
                    break;
                case "2":
                    showAllCourses();
                    break;
                case "3":
                    showCourseDetails();
                    break;
                case "4":
                    updateCourseDetails();
                    break;
                case "5":
                    deleteCourse();
                    break;
                case "6":
                    searchCourse();
                    break;
                case "7":
                    runningCourses();
                    break;
                case "x":
                    start();
                    break;
                default:
                    inputError();
                    break;
            }
        }
    }


    private void runningCourses() {
        System.out.println("Aktuell laufende Kurse:");
        List<Course> list;
        try {
            list = courseRepo.findAllRunningCourses();
            for (Course course : list) {
                System.out.println(course);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Kursanzeige für laufende Kurse: " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler bei Kursanzeige für laufende Kurse: " + e.getMessage());
        }
    }

    private void searchCourse() {

        System.out.println("Geben Sie eineen Suchbegriff an!");
        String searchString = scan.nextLine();
        List<Course> courseList;
        try {
            courseList = courseRepo.findAllCoursesByNameOrDescription(searchString);
            for (Course course : courseList) {
                System.out.println(course);
            }

        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei der Kurssuche: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei der Kurssuche: " + exception.getMessage());
        }
    }

    private void deleteCourse() {
        System.out.println("Welchen Kurs möchten Sie löschen? Bitte ID eingeben:");
        Long courseIdToDelete = Long.parseLong(scan.nextLine());

        try {
            courseRepo.deleteById(courseIdToDelete);
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Löschen: " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler beim Löschen: " + e.getMessage());
        }
    }

    private void updateCourseDetails() {
        System.out.println("Für welchee Kurs-ID möchten Sie die Kursdetails ändern?");
        Long courseId = Long.parseLong(scan.nextLine());

        try {
            Optional<Course> courseOptional = courseRepo.getById(courseId);
            if (courseOptional.isEmpty()) {
                System.out.println("Kurs mit der gegebenen ID nicht in der Datenbank!");
            } else {
                Course course = courseOptional.get();

                System.out.println("Änderungen für folgenden Kurs: ");
                System.out.println(course);

                String name, description, hours, dateFrom, dateTo, courseType;

                System.out.println("Bitte neue Kursdaten angeben (Enter, falls keine Änderung gewünscht ist):");
                System.out.println("Name:");
                name = scan.nextLine();
                System.out.println("Beschreibung:");
                description = scan.nextLine();
                System.out.println("Stundenanzahl:");
                hours = scan.nextLine();
                System.out.println("Startdatum (YYYY-MM-DD):");
                dateFrom = scan.nextLine();
                System.out.println("Enddatum (YYYY-MM-DD):");
                dateTo = scan.nextLine();
                System.out.println("Kurstyp: (ZA/BF/FF/OE)");
                courseType = scan.nextLine();

                Optional<Course> optionalCourseUpdated = courseRepo.update(
                        new Course(
                                course.getId(),
                                name.equals("") ? course.getName() : name,
                                description.equals("") ? course.getDescription() : description,
                                hours.equals("") ? course.getHours() : Integer.parseInt(hours),
                                dateFrom.equals("") ? course.getBeginDate() : Date.valueOf(dateFrom),
                                dateTo.equals("") ? course.getEndDate() : Date.valueOf(dateFrom),
                                courseType.equals("") ? course.getCourseType() : CourseType.valueOf(courseType)
                        )
                );
                optionalCourseUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Kurs aktualisiert: " + c),
                        () -> System.out.println("Kurs konnte nicht aktualisiert werden!")
                );
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Kursdaten nicht korrekt angegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Aktualisieren: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Aktualisieren: " + exception.getMessage());
        }
    }

    private void addCourse() {
        String name, description;
        int hours;
        Date dateFrom, dateTo;
        CourseType courseType;

        try {
            System.out.println("Bitte alle Kursdaten angeben:");
            System.out.println("Name:");
            name = scan.nextLine();
            if (name.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Beschreibung:");
            description = scan.nextLine();
            if (description.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Stundenanzahl:");
            hours = Integer.parseInt(scan.nextLine());
            System.out.println("Startdatum (YYYY-MM-DD):");
            dateFrom = Date.valueOf(scan.nextLine());
            System.out.println("Enddatum (YYYY-MM-DD):");
            dateTo = Date.valueOf(scan.nextLine());
            System.out.println("Kurstyp: (ZA/BF/FF/OE)");
            courseType = CourseType.valueOf(scan.nextLine());

            Optional<Course> optionalCourse = courseRepo.insert(new Course(name, description, hours, dateFrom, dateTo, courseType));

            if (optionalCourse.isPresent()) {
                System.out.println("Kurs angelegt: " + optionalCourse.get());
            } else {
                System.out.println("Kurs konnte nicht angelegt werden!");
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Kursdaten nicht korrekt angegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void showCourseDetails() {
        System.out.println("Für welchen Kurs möchten Sie die Kursdetails anzeigen?");
        Long courseId = Long.parseLong(scan.nextLine());

        try {
            Optional<Course> courseOptional = courseRepo.getById(courseId);
            if (courseOptional.isPresent()) {
                System.out.println(courseOptional.get());
            } else {
                System.out.println("Kurs mit der ID " + courseId + " nicht gefunden!");
            }

        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Kurs-Detailanzeige: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Kurs-Detailanzeige: " + exception.getMessage());
        }
    }

    private void showAllCourses() {
        List<Course> list = null;

        try {
            list = courseRepo.getAll();
            if (list.size() > 0) {
                for (Course course : list) {
                    System.out.println(course);
                }
            } else {
                System.out.println("Kursliste leer!");
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Anzeige aller Kurse: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige aller Kurse: " + exception.getMessage());
        }
    }



    private void searchStudentByPrename() {
        System.out.println("Geben Sie eineen Suchbegriff an!");
        String prename = scan.nextLine();
        List<Student> studentList;
        try {
            studentList = studentRepo.findAllStudentsByPrename(prename);
            for (Student student : studentList) {
                System.out.println(student);
            }

        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei der Studentensuche: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei der Studentensuche: " + exception.getMessage());
        }
    }

    private void deleteStudent() {
        System.out.println("Welchen Student möchten Sie löschen? Bitte ID eingeben:");
        Long studentIdToDelete = Long.parseLong(scan.nextLine());

        try {
            studentRepo.deleteById(studentIdToDelete);
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Löschen: " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler beim Löschen: " + e.getMessage());
        }
    }

    private void updateStudentDetails() {
        System.out.println("Für welche Studenten-ID möchten Sie die Kursdetails ändern?");
        Long courseId = Long.parseLong(scan.nextLine());

        try {
            Optional<Student> courseOptional = studentRepo.getById(courseId);
            if (courseOptional.isEmpty()) {
                System.out.println("Student mit der gegebenen ID nicht in der Datenbank!");
            } else {
                Student student = courseOptional.get();

                System.out.println("Änderungen für folgenden Student: ");
                System.out.println(student);

                String prename, lastname, birthdate;

                System.out.println("Bitte neue Studentendaten angeben (Enter, falls keine Änderung gewünscht ist):");
                System.out.println("Vorname:");
                prename = scan.nextLine();
                System.out.println("Nachname:");
                lastname = scan.nextLine();
                System.out.println("Geburtsdatum (YYYY-MM-DD):");
                birthdate = scan.nextLine();

                Optional<Student> optionalStudentUpdated = studentRepo.update(
                        new Student(
                                student.getId(),
                                prename.equals("") ? student.getPrename() : prename,
                                lastname.equals("") ? student.getLastname() : lastname,
                                birthdate.equals("") ? student.getBirthdate() : Date.valueOf(birthdate)

                        )
                );
                optionalStudentUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Student aktualisiert: " + c),
                        () -> System.out.println("Student konnte nicht aktualisiert werden!")
                );
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Studentendaten nicht korrekt angegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Aktualisieren: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Aktualisieren: " + exception.getMessage());
        }
    }

    private void showStudentDetails() {
        System.out.println("Welchen Student möchten Sie anzeigen?");
        Long studentId = Long.parseLong(scan.nextLine());

        try {
            Optional<Student> studentOptional = studentRepo.getById(studentId);
            if (studentOptional.isPresent()) {
                System.out.println(studentOptional.get());
            } else {
                System.out.println("Student mit der ID " + studentId + " nicht gefunden!");
            }

        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Studentenanzeige: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Studentenanzeige: " + exception.getMessage());
        }
    }

    private void showAllStudents() {
        List<Student> list = null;

        try {
            list = studentRepo.getAll();
            if (list.size() > 0) {
                for (Student student : list) {
                    System.out.println(student);
                }
            } else {
                System.out.println("Studentenliste leer!");
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Anzeige aller Studenten: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige aller Studenten: " + exception.getMessage());
        }
    }

    private void addStudent() {
        String prename, lastname;
        Date birthdate;


        try {
            System.out.println("Bitte alle Studentendaten angeben:");
            System.out.println("Name:");
            prename = scan.nextLine();
            if (prename.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Beschreibung:");
            lastname = scan.nextLine();
            if (lastname.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");

            System.out.println("Geburtsdatum (YYYY-MM-DD):");
            birthdate = Date.valueOf(scan.nextLine());


            Optional<Student> optionalStudent = studentRepo.insert(new Student(prename, lastname, birthdate));

            if (optionalStudent.isPresent()) {
                System.out.println("Student angelegt: " + optionalStudent.get());
            } else {
                System.out.println("Student konnte nicht angelegt werden!");
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Kursdaten nicht korrekt angegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }
    }

    private void inputError() {
        System.out.println("Bitte die Zahl der Menüeingabe eingaben!\n");
    }

    private void showMenue() {
        System.out.println("----------------- KURSMANAGEMENT -----------------");
        System.out.println("(1) Kursverwaltung \t (2) Studentenverwaltung \t (3) xxxx");
        System.out.println("(x) ENDE");

    }

    private void showCourseMenue() {
        System.out.println("----------------- Kursverwaltung -----------------");
        System.out.println("(1) Kurs eingeben \t (2) Alle Kurse anzeigen \t (3) Kursdetails anzeigen");
        System.out.println("(4) Kursdetails ändern \t (5) Kurs löschen \t (6) Kurs suchen");
        System.out.println("(7) Laufende Kurse \t (-) xxxxxx \t (-) xxxxxx");
        System.out.println("(x) ZURÜCK ZUM HAUPTMENÜ");
    }

    private void showStudentMenue() {
        System.out.println("----------------- Studentenverwaltung -----------------");
        System.out.println("(1) Student hinzufügen \t (2) Alle Studenten anzeigen \t (3) Student anzeigen");
        System.out.println("(4) Student ändern \t (5) Student löschen \t (6) Student suchen");
        System.out.println("(x) ZURÜCK ZUM HAUPTMENÜ");

    }
}
