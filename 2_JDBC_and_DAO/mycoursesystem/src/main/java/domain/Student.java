package domain;

import java.sql.Date;

public class Student extends BaseEntity {

    private String prename;
    private String lastname;
    private Date birthdate;

    public Student(Long id, String prename, String lastname, Date birthdate) {
        super(id);
        this.setPrename(prename);
        this.setLastname(lastname);
        this.setBirthdate(birthdate);
    }

    public Student(String prename, String lastname, Date birthdate) {
        super(0L);
        this.setPrename(prename);
        this.setLastname(lastname);
        this.setBirthdate(birthdate);
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        if (prename != null && prename.length() > 1) {
            this.prename = prename;
        } else {
            throw new InvalidValueException("Der Vorname muss mindestens 2 Zeichen lang sein!");
        }
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        if (lastname != null && lastname.length() > 1) {
            this.lastname = lastname;
        } else {
            throw new InvalidValueException("Der Nachname muss mindestens 2 Zeichen lang sein!");
        }
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        if (birthdate != null) {
            this.birthdate = birthdate;
        } else {
            throw new InvalidValueException("Geburtsdatum darf nicht null / leer sein!");
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + this.getId() + '\'' +
                "prename='" + prename + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birthdate=" + birthdate +
                '}';
    }
}
