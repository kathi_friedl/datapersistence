# **Datenpersistenz**


## **Task 1**: JDBC INTRO TEIL 1

Arbeite das JDBC-Intro-Videotraining aus dem Kurs vollständig durch
- Richte die Entwicklungsumgebung ein
- Programmiere alles selbstständig nach und mit (!)
- Spiele ein wenig mit dem Code, probiere aus!
- Du musst alle Codeteile verstehen und syntaktisch sowie semantisch erklären können.
- Du musst neue Konzepte verstehen und erklären können und diese selbst dokumentieren
(Markdown), dazu zählen:
  - MySQL-Datenbankserver einreichten
  - DB-Server Adminkonsole einreichten und verwenden können (insb. auch SQL-Statements
absetzen können)
  - Java-Maven-Projekt erstellen
  - Dependency für die Verbindung einer MySQL-Datenbank in der pom.xml hinzufügen
  - Verbindung zur Datenbank aufbauen
  - Prepared-Statement für die Abfrage von Daten aus der DB verwenden
  - Prepared-Statement für die Änderung von Daten in der DB verwenden
  - Abgefragte Daten aus der DB mit ResultSet und Schleifen verarbeiten
  - Exceptions verwenden und verarbeiten, die auftreten können
  - Debugging von JDBC-Applikationen (SQL-Statements prüfen, Exceptions richtig interpretieren, Debugger verwenden)

### [Zur Dokumentation](./1_JDBC_Intro/README.md)
---


## **Task 2**: JDBC INTRO TEIL 2

Erstelle neben der Tabelle im Videotraining eine weitere Tabelle mit Primärschlüssel und mindestens 2 weiteren Spalten (eine Textspalte, eine Zahlenspalte) für eine weitere Entität nach Wahl (z.B. Kurs, Adresse, Hobbies, etc.)

### [Zur Dokumentation](./1_JDBC_Intro/new_table_phpmyadmin/README.md)
---


## **Task 3**: JDBC UND DAO – KURSE

Lerne anhand des zweiten Videotrainings, wie man mit JAVA auf Daten aus Datenbanken mit dem DAOEntwurfsmuster zugreift.
- Programmiere alles selbstständig nach und mit!
- Spiele ein wenig mit dem Code, probiere aus!• Du musst alle Codeteile verstehen und syntaktisch sowie semantisch erklären können
- Du musst neue Konzepte verstehen und erklären können und diese selbst dokumentieren (Markdown). Zu diesen Konzepten zählen:
  - DAO-Pattern zum objektrelationalen Zugriff auf Datenbanken verstehen und anwenden
  - Grundkonzept des objektrelationalen Mappings verstehen
  - Singleton-Pattern zum Aufbau der DB-Verbindung verstehen und anwenden
  - Commandline-Interface (Kommandozeilenmenü) sauber programmieren
  - Exceptions verstehen und verwenden
  - Abstrakte Klassen verstehen und verwenden
  - Interfaces (auch mit Erben für Interfaces) verstehen und verwenden
  - Domänenklassen korrekt aufbauen (Objekte immer im konsistenten Zustand halten, Exceptions verwenden, Setter absichern)
  - CRUD-Operationen mit DAO-Pattern und JDBC umsetzen


### [Zur Dokumentation](./2_JDBC_and_DAO/README.md)
---

## **Task 4**: JDBC UND DAO – STUDENTEN

Erweitere die fertig nachprogrammierte Applikation mit einem DAO für CRUD für eine neue Domänenklasse „Student“:
- Studenten haben einen eine Student-ID, einen VN, einen NN, ein Geburtsdatum
- Domänenklasse implementieren (Setter absichern, neue Exceptions definieren, Business-Regeln selbst wählen - z.B. dass Name nicht leer sein darf)
- Eigenes Interface MyStudentRepository von BaseRepository ableiten. MyStudentRepository muss mindestens 3 studentenspezifische Methoden enthalten (z. B. Studentensuche nach Namen, Suche nach ID, Suche nach bestimmtem Geburtsjahr, Suche mit Geburtsdatum zwischen x und y etc.).
- Implementierung von MyStudentRepository durch eine neue Klasse MySqlStudentRepository analog
- Erweiterung des CLI für die Verarbeitung von Studenten und für spezifische Studenten-Funktionen (z.B. Student nach dem Namen suchen)

### [Zur Dokumentation](./2_JDBC_and_DAO/README.md)
---

## **Task 5**: JDBC UND DAO – BUCHUNGEN

Gib einen textuellen Vorschlag hab, wie man die bisher programmierte Applikation für die Buchung von Kursen durch Studenten erweitern könnte.

Beschreibe, wie eine neue Buchungs-Domänenklasse ausschauen sollte, wie man ein DAO für Buchungen dazu entwickeln sollte, wie man die CLI anpassen müsste und welche Anwendungsfälle der Benutzer brauchen könnte (wie etwa „Buchung erstellen“).

Verwende zur Illustration insb. auch UML-Diagramme.

### [Zur Dokumentation](./2_JDBC_and_DAO/README.md)


