import java.sql.*;

public class JdbcDemo {

    public static void main(String[] args) {
        System.out.println("JDBC Demo!");
        selectAllDemo();
        insertStudentDemo("Peter Zeck", "p.zeck@hotmail.com");
        selectAllDemo();
        updateStudentDemo(3, "Herrmann Fischer", "hefisch@tsn.at");
        selectAllDemo();
        deleteStudentDemo(3);
        selectAllDemo();

        findAllByName("ka");
    }

    public static void insertStudentDemo(String name, String email) {

        System.out.println("INSERT DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pw = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user, pw)) {
            System.out.println("Verbindung zur DB hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO `student` (`id`, `name`, `email`) VALUES (NULL, ?, ?)");
            try {
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, email);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze eingefügt");
            } catch (SQLException e) {
                System.out.println("Fehler im SQL-INSERT Statement: " + e.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }

    public static void updateStudentDemo(int id, String neuerName, String neueEmail) {

        System.out.println("UPDATE DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pw = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user, pw)) {
            System.out.println("Verbindung zur DB hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement("UPDATE `student` SET `name` = ?, `name` = ? WHERE `student`.`id` = ?");
            try {
                preparedStatement.setString(1, neuerName);
                preparedStatement.setString(2, neueEmail);
                preparedStatement.setInt(3, id);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println("Anzahl der aktualisierten Datensätze: " + rowAffected);
            } catch (SQLException e) {
                System.out.println("Fehler im SQL-UPDATE Statement: " + e.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }

    private static void findAllByName(String pattern) {
        System.out.println("Find all by Name DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pw = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user, pw)) {

            System.out.println("Verbindung zur DB hergestellt!");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM `student` WHERE `student`.`name` LIKE ?");
            preparedStatement.setString(1, "%" + pattern + "%");
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der DB: [ID] " + id + " [NAME] " + name + " [EMAIL] " + email);
            }

        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }


    public static void selectAllDemo() {

        System.out.println("Select DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pw = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user, pw)) {

            System.out.println("Verbindung zur DB hergestellt!");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM `student`");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der DB: [ID] " + id + " [NAME] " + name + " [EMAIL] " + email);
            }

        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }

    public static void deleteStudentDemo(int studentId) {

        System.out.println("UPDATE DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pw = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user, pw)) {
            System.out.println("Verbindung zur DB hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM `student` WHERE `student`.`id` = ?");
            try {
                preparedStatement.setInt(1, studentId);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println("Anzahl gelöschten Datensätze: " + rowAffected);
            } catch (SQLException e) {
                System.out.println("Fehler im SQL-DELETE Statement: " + e.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }

}
