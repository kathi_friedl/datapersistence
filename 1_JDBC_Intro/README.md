
# **JDBC Intro** 

### **Aufgabe 1**
Arbeite das JDBC-Intro-Videotraining aus dem Kurs vollständig durch
- Richte die Entwicklungsumgebung ein
- Programmiere alles selbstständig nach und mit (!)
- Spiele ein wenig mit dem Code, probiere aus!
- Du musst alle Codeteile verstehen und syntaktisch sowie semantisch erklären können.
- Du musst neue Konzepte verstehen und erklären können und diese selbst dokumentieren
(Markdown), dazu zählen:
  - MySQL-Datenbankserver einreichten
  - DB-Server Adminkonsole einreichten und verwenden können (insb. auch SQL-Statements
absetzen können)
  - Java-Maven-Projekt erstellen
  - Dependency für die Verbindung einer MySQL-Datenbank in der pom.xml hinzufügen
  - Verbindung zur Datenbank aufbauen
  - Prepared-Statement für die Abfrage von Daten aus der DB verwenden
  - Prepared-Statement für die Änderung von Daten in der DB verwenden
  - Abgefragte Daten aus der DB mit ResultSet und Schleifen verarbeiten
  - Exceptions verwenden und verarbeiten, die auftreten können
  - Debugging von JDBC-Applikationen (SQL-Statements prüfen, Exceptions richtig interpretieren, Debugger verwenden)

---

<br>


## **1 Einrichtung der Entwicklungsumbgebung**

1. Webserver
2. Datenbankserver
3. IDE
4. Maven-Projektsetup

### **MySQL-Datenbankserver einrichten**

Die einfachste Variante bietet hier das Programm XAMPP, welches kostenlos für verschiedene Betriebssysteme heruntergeladen werden kann.
- Nach der Installation können die Services für Apache als Webserver und MySQL als Datenbank gestartet werden.
- Unter dem Link localhost/phpmyadmin gelangt man zum Datenbankserver

### **Java-Maven-Projekt erstellen**

In der IDE IntelliJ kann ein Maven-Projekt erstellt werden.
- New Project -> Maven (Project SDK: neueste (aktuell 17))
  - Name: jdbcdemo
  - GroupId: at.itkolleg
  - ArtifactId: jdbcdemo

### **Dependency für die Verbindung einer MySQL-Datenbank in der pom.xml hinzufügen**

In der Datei "pom.xml" Dependency einfügen:

    ```
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>8.0.28</version>
    </dependency>
    ```
![Maven-Dependency](./img/dependency.jpg)

Mit einer neuen Klasse JdbcDemo, in der sich die Main-Methode befindet, kann getestest werden, ob alles funktioniert.

## **2 Datenbankverbindung**

Datenbank mit Tabellen in phphMyAdmin erstellen:
- Neu -> Datenbankname eingeben (jdbcdemo) -> Anlegen
- Tabellen hinzufügen:
  - Tabelle "student": id (INT, PRIMARY, AUTO_INCREMENT), name (VARCHAR (200)), email (VARCHAR (200))
  - Datensätze zum Testen einfügen

### **Verbindung zur Datenbank aufbauen**

Methode für die Datenbank-Verbindung erstellen:

```
public static void selectAllDemo(){

    System.out.println("Select DEMO mit JDBC");
    String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
    String user = "root";
    String pw ="";

    try(Connection conn = DriverManager.getConnection(connectionUrl,"root", "")){
        System.out.println("Verbindung zur DB hergestellt!");
    }catch (SQLException e){
        System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
    }
}
```

Man holt sich aus dem `DriverManager` mit `DriverManager.getConnection()` eine Datenbankverbindung vom Typ `Connection`, gibt als Parameter die URL zur Datenbank `connectionUrl`, den Benutzer `user` und das Passwort `pw` mit und bekommt dann eine Verbindung zurück.


## **3 Daten abfragen**

### **Daten anzeigen**

Prepared-Statement erstellen und ausführen:
```
PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM `student`");
ResultSet rs = preparedStatement.executeQuery();
```

Wir holen uns vom Connection-Object `conn` ein Prepared-Statement, welches in die Variable `preparedStatement` vom Typ `PreparedStatement` gespeichert wird. Beim Herstellen des Prepared-Statements muss der SQL-String mitgegeben werden, welcher an die Datenbank gesendet werden soll. Durch den Aufruf von `preparedStatement.executeQuery()`, wodurch die SQL-Abfrage des Prepared-Statements ausgeführt wird, bekommen wir aus der Datenbank eine Ergebnismenge vom Typ `ResultSet` zurück.

```
while(rs.next()){
    int id = rs.getInt("id");
    String name = rs.getString("name");
    String email = rs.getString("email");
    System.out.println("Student aus der DB: [ID] " + id + " [NAME] " + name + " [EMAIL]");
}
```

Das ResultSet kann anschließend mit einem Zeiger durchlaufen werden: <br>
Der Ausdruck `rs.next()`, als Bedingung der while-Schleife, gibt `true` zurück, solange im ResultSet `rs` ein "nächster Datensatz" vorhanden ist. Gleichzeitig wird auch auf den jeweiligen Datensatz im ResultSet gezeigt. Es können somit Daten aus den gewünschten Spalten des entsprechenden Datensatzes geholt werden. Dies erfolgt durch den Aufruf verschiedener Getter-Methoden an der ResultSet-Variablen `rs`. <br><br>

Methode zum Anzeigen aller Datensätze:


```
    public static void selectAllDemo() {

        System.out.println("Select DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pw = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user, pw)) {

            System.out.println("Verbindung zur DB hergestellt!");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM `student`");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der DB: [ID] " + id + " [NAME] " + name + " [EMAIL] " + email);
            }

        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }
```




### **Daten einfügen**
Methode zum Einfügen:

```
    public static void insertStudentDemo(String name, String email) {

        System.out.println("INSERT DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pw = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user, pw)) {
            System.out.println("Verbindung zur DB hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO `student` (`id`, `name`, `email`) VALUES (NULL, ?, ?)");
            try {
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, email);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze eingefügt");
            } catch (SQLException e) {
                System.out.println("Fehler im SQL-INSERT Statement: " + e.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }
```

**Prepared-Statements** <br>
Prepared-Statements werden schon vorkompiliert in der Datenbank abgelegt. Beim eigentlichen Ausführen des Statements werden nur noch die Datenwerte übergeben. Das hat sicherheitstechnische Vorteile, da SQL-Injections vermieden werden können und somit beispielsweise ungewolltes Löschen und Ändern von Daten durch Benutzereingaben verhindert werden kann.



### **Daten ändern**
Methode zum Ändern:

```
    public static void updateStudentDemo(int id, String neuerName, String neueEmail) {

        System.out.println("UPDATE DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pw = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user, pw)) {
            System.out.println("Verbindung zur DB hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement("UPDATE `student` SET `name` = ?, `name` = ? WHERE `student`.`id` = ?");
            try {
                preparedStatement.setString(1, neuerName);
                preparedStatement.setString(2, neueEmail);
                preparedStatement.setInt(3, id);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println("Anzahl der aktualisierten Datensätze: " + rowAffected);
            } catch (SQLException e) {
                System.out.println("Fehler im SQL-UPDATE Statement: " + e.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }
```


### **Daten löschen**
Methode zum Löschen:

```
    public static void deleteStudentDemo(int studentId) {

        System.out.println("UPDATE DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pw = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user, pw)) {
            System.out.println("Verbindung zur DB hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM `student` WHERE `student`.`id` = ?");
            try {
                preparedStatement.setInt(1, studentId);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println("Anzahl gelöschten Datensätze: " + rowAffected);
            } catch (SQLException e) {
                System.out.println("Fehler im SQL-DELETE Statement: " + e.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }
```


### **Daten nach bestimmten Bedingungen ausgeben**
Methode zum ausgeben der Datensätze mit einem bestimmten String als inhalt.

```
private static void findAllByName(String pattern) {
        System.out.println("Find all by Name DEMO mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pw = "";

        try (Connection conn = DriverManager.getConnection(connectionUrl, user, pw)) {

            System.out.println("Verbindung zur DB hergestellt!");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM `student` WHERE `student`.`name` LIKE ?");
            preparedStatement.setString(1, "%" + pattern + "%");
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der DB: [ID] " + id + " [NAME] " + name + " [EMAIL] " + email);
            }

        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }
```


