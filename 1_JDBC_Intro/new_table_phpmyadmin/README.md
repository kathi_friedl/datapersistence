### **Aufgabe 2**

Erstelle neben der Tabelle im Videotraining eine weitere Tabelle mit Primärschlüssel und mindestens 2 weiteren Spalten (eine Textspalte, eine Zahlenspalte) für eine weitere Entität nach Wahl (z.B. Kurs, Adresse, Hobbies, etc.)



```
CREATE TABLE `adresse` (
  `id` int(11) NOT NULL,
  `strasse` varchar(200) NOT NULL,
  `hausnummer` int(11) NOT NULL,
  `postleitzahl` varchar(200) NOT NULL,
  `ort` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `adresse`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `adresse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
```


![Tabelle Adresse](./img/newTableAddress.png)


